"use strict"

// Теоретичне питання
// 1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Ответ: Есть обьект (Родитель), и когда нам нужно создать новые обьекты, их прототип берет за основу прототип родителя, и выходит, что у обьекта-ребенка уже есть и свой прототип, и прототип родителя (мы создаем новый обьект на основе родителя). Реализация подобных вещей происходит благодаря Классам.

// 2.Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Ответ: Благодаря super() мы получаем все свойства класса-родителя. Блягодаря super. мы получаем его методы


class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get nameEmployee() {
    return this._name;
  }
  set nameEmployee(name) {
    this._name = name;
  }

  get ageEmployee() {
    return this._age;
  }
  set ageEmployee(age) {
    this._age = age;
  }

  get salaryEmployee() {
    return this._salary;
  }
  set salaryEmployee(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor( name, age, salary, lang) { 
    super(name, age, salary);
    this.lang = lang;

    
  }
  get salaryEmployee() {
    return this._salary * 3;
    
}






}

let firstProgrammer = new Programmer( "Artem", 32, 200, "C++");
let secondProgrammer = new Programmer("Maxim", 23, 3400, "JS");

console.log(firstProgrammer);
console.log(secondProgrammer);


