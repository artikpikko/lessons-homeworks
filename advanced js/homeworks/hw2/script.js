// Теоретичне питання

//  Например: когда можно вывести ошибку на страницу браузера с помощью try/catch для юзера, если где-то код поломался. В таком случае , он будет осведомлен об этом.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class KeyMissedError extends Error {
  constructor(key) {
    super();
    this.name = "KeyMissedError";
    this.message = `This ${key} is missed"`;
  }
}

class Books {
  constructor(book) {
    if (!book.author) {
      throw new KeyMissedError('author');
    }
    else if (!book.name) {
      throw new KeyMissedError('name');
    }
    else if (!book.price) {
      throw new KeyMissedError('price');
    }

    this.author = book.author;
    this.name = book.name;
    this.price = book.price;
  }

  render(container) {
    container.insertAdjacentHTML(
      "beforeend",
      `<ul>
      <li>Author: ${this.author}</li>
      <li>Name: ${this.name}</li>
      <li>Price: ${this.price}</li>
    </ul>`
    );
  }
}

/* <ul>
      <li></li>
      <li></li>
      <li></li>
  </ul> */

const container = document.querySelector("#root");

books.forEach((e) => {
  try {
    new Books(e).render(container);
  } catch (err) {
    if (err.name === "KeyMissedError") {
      console.warn(err);
    } else {
      throw err;
    }
  }
});
