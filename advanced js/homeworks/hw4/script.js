// Теоретический вопрос

// AJAX помогает в заимодействии с запросами получения и отправки данных (архетиктура клиент - сервер) без перезагрузки страницы браузера, это ускоряет работу как пользователя, так и браузера

const url = "https://ajax.test-danit.com/api/swapi/films";

fetch(url)
  .then((response) => response.json())
  .then((data) => {
    data.forEach(({ episodeId, name, openingCrawl, characters }) => {
      document.querySelector(".container").insertAdjacentHTML(
        "beforeend",
        `<div class="episode ${episodeId}">
    <h2>Episode ${episodeId} ${name}</h2>
    <h3 class="episode${episodeId}">Characters: </h3>
    <p>${openingCrawl}</p>
    
    </div>`
      );

      characters.forEach((link) =>
        fetch(link)
          .then((response) => response.json())
          .then((data) => {
            document
              .querySelector(`.episode${episodeId}`)
              .insertAdjacentHTML(
                "beforeend",
                `<p class="character">${data.name}, </p>`
              );
          })
      );
    });
  })
  .catch((err) => alert("URL not found"));
