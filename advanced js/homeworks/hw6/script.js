const container = document.querySelector(".container");
const btn = document.createElement("button");
btn.innerText = "Find by IP";
const renderIpInfo = document.createElement("div");
container.append(btn);
const url = "https://api.ipify.org/?format=json";

btn.addEventListener("click", findIp);

async function findIp() {
  const { ip } = await fetch(url).then(response => response.json());

  await ipInfo();

  async function ipInfo() {
    const urlInfo = `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`;
    const { city, continent, country, district, regionName } = await fetch(urlInfo).then(response => response.json());
    // .then((data) => console.log(data))
    renderIpInfo.innerHTML = `Континент: ${continent}, Країна: ${country}, Місто: ${city}, Регіон: ${regionName}, Район:${district}`;
    btn.after(renderIpInfo);
  }
}
