// Теоретический вопрос
// 1.Экранирование. В JS много служебных символов отвечают за определенные действия, например кавычки дают нам строку, знак равно - присвоение переменной, но если, например, нам нужно эти символы использовать внутри строки, чтобы они не конфликтовали с кодом используется \ перед символом, например.

// let str1 = 'Hello'  выведет Hello 
// let str2 = '"Hello"'  выведет ошибку
// let str3 = '\"Hello\"'    выведет "Hello"

function createNewUser() {
  let userName = prompt("Enter your name");
  let userLastName = prompt("Enter your last name");
  let birthday = prompt( "Enter your birth date", "1990,10,10");
  

  let newUser = {
    firstName: userName,
    lastName: userLastName,
    

    getLogin: function () {
      let newLogin =
        newUser.firstName.charAt(0).toLowerCase() +
        newUser.lastName.toLowerCase();
      return newLogin;
    },

    getAge: function () {
      let currentDate = new Date();
      let currentYear = birthday.split(",", 1)
      let userAge = currentDate.getFullYear() - currentYear;
      return userAge;
    },
    getPassword: function () {
      let newPassword =
        newUser.firstName.charAt(0).toUpperCase() +
        newUser.lastName.toLowerCase() +
       newUser.getAge();
      return newPassword;
      
    },
  };
  alert(`Login  ${newUser.getLogin()}`);
  console.log(`Login  ${newUser.getLogin()}`);
  console.log(`Your birthday ${newUser.getAge()}`);
  console.log(`Your password ${newUser.getPassword()}`);
}

createNewUser();
