// Теоретический вопрос

// 1. var устаревший способ создания переменных, после выхода ES6 появились новые операторы: let и const
// Разница между ними в том, что переменные, котоыре мы обьявляем через const не могут изменятся  в дальнейшем, в отличии от let.

//2. по сути, ответ в первом вопросе.

let userName = prompt("What is your name?");
let userAge = +prompt("How old are you?");

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge > 22) {
  alert(`Welcome, ${userName}`);
} else if (userAge >= 18 || userAge <= 22) {
  let userQuestion = confirm("Are you sure you want to continue?");
  if (userQuestion === true) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert("You are not allowed to visit this website");
}
