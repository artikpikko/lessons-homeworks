let tab = document.querySelectorAll('.tabs li');
let tabContent = document.querySelectorAll('.tabs-content li');


for(let i = 0; i < tab.length; i++){
    tab[i].classList.add('tabs-title');
    
    tab[i].addEventListener('click',function(){
      for( let u = 0; u < tab.length; u++){
        tabContent[u].style.display = 'none';
        tab[u].classList.remove('active');
      }

      tabContent[i].style.display = 'block';
      this.classList.add('active');
    });
  }

