const theme = {
  dark: {
    menuColor: "#000",
    backgroundColor: "#4F4E4E",
  },

  light: {
    menuColor: "#fff",
    backgroundColor: "#fff",
  },
};

window.onload = () => {
  const isTheme = !!localStorage.getItem("theme");
  !isTheme && localStorage.setItem("theme", "light");
  changeTheme()
};

const themeBtn = document.querySelector(".btn-theme");
const setNewTheme = () => {
  const currentTheme = localStorage.getItem("theme");
  const newTheme = currentTheme === "light" ? "dark" : "light";
  localStorage.setItem("theme", newTheme);
};

const changeTheme = () => {
  const currentTheme = localStorage.getItem("theme");
  const listMenu = document.querySelectorAll("li");
  const mainContainer = document.querySelectorAll(".container");

  listMenu.forEach((item) => {
    item.style.color = theme[currentTheme].menuColor;
  });

  mainContainer.forEach((item) => {
    item.style.background = theme[currentTheme].backgroundColor;
  });
};

themeBtn.addEventListener("click", setNewTheme);
themeBtn.addEventListener("click", changeTheme);
