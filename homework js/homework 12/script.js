// #Теоретический вопрос
// 1. setTimeout срабатывает лиш раз, когда setInterval  повторяет свое действие;
// 2. не сразу, setTimeout попадет в конец очереди стека.
// 3. если мы используем setInterval , мы должны вызвать clearInterval(), что-бы остановить его работу



let start = document.querySelector('.btn-start');
let stop = document.querySelector('.btn-stop');
let images = document.querySelectorAll('.image-to-show');

images.forEach((e) => {
    e.style.display ='none'
})
images.item(0).style.display = 'inline'

let i = 1;
function showImages() {
    images[i-1].style.display ='none'
    if(i >=images.length) {
        i = 0;
    }
    images[i].style.display ='inline'
    i++
}

let timerId = setInterval(showImages, 3000)

start.addEventListener('click', () => {
    timerId = setInterval(showImages, 3000)

});

stop.addEventListener('click', () => { 
    clearInterval(timerId)  
})


