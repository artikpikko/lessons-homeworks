 // Теоретический вопрос
// 1. Метод .forEach  очень похож по аналогии с for(), только для массива. Метод используют для перебора массива.
// Работает таким образом  - этот метод запускает функцию для каждого элемента в массиве, при этом в  функцию можно передать 3 параметра - елемент массива, его индекс и сам массив  
// .forEach(function(элемент, индекс, массив)){}


function filterBy(arr, type) {
  return arr.filter((item) => typeof item !== type)

// //   return arr.filter(function (item) {
// //     if (typeof item !== type) {
// //       return item;

// //     }
// //   }
// // )

}

console.log(filterBy([4, 2, "hey"], "string"));




