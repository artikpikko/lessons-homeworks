//Теоретический вопрос

// 1.Когда браузер загружает html документ, он на его основании создает DOM - дерево елементов, со своими свойствами, методами. И благодаря этому, мы можем с помощью JS добраться до елементов html.



const arrayList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const parent = document.getElementById("block-list");

function createList(array, parent) {
  const ul = document.createElement("ul");
  parent.append(ul);
  const content = array
    .map((element) => {
      return `<li>${element}</li>`;
    })
    .join("");

  ul.innerHTML = content;
}

createList(arrayList, parent);
